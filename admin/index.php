<?php require '../connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RC | Admin</title>
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
</head>
<body>

    <div class="container" id="cont-1">
    <h1>MOBIL</h1>
        <form method="POST" enctype="multipart/form-data" action="add_proses.php" class="form col-md-6 border border-success rounded" id="form">
            <label>Nama Barang</label>
            <input name="nama" type="text" class="form-control" required="required">
            <label>Plat Kendaraan</label>
            <input name="plat" type="text" class="form-control" required="required">
            <label>Harga</label>
            <input name="harga" type="text" class="form-control" required="required">
            <label>Gambar</label>
            <input name="image" type="file" class="form-control" required="required">
            <button class="btn btn-primary" name="add">Unggah</button></br></br>
        </form>

    <h1>MOTOR</h1>
        <form method="POST" enctype="multipart/form-data" action="add_proses_2.php" class="form col-md-6 border border-success rounded" id="form">
            <label>Nama Barang</label>
            <input name="nama" type="text" class="form-control" required="required">
            <label>Plat Kendaraan</label>
            <input name="plat" type="text" class="form-control" required="required">
            <label>Harga</label>
            <input name="harga" type="text" class="form-control" required="required">
            <label>Gambar</label>
            <input name="image" type="file" class="form-control" required="required">
            <button class="btn btn-primary" name="add">Unggah</button></br></br>
        </form>

    </div>
</body>
</html>