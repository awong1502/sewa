<?php	
	require_once '../connect.php';
	
	if(ISSET($_POST['add'])){
		$nama = $_POST['nama'];
        $plat = $_POST['plat'];
        $price = $_POST['harga'];
		$file_name = $_FILES['image']['name'];
		$file_temp = $_FILES['image']['tmp_name'];
		$allowed_ext = array("jpg", "jpeg", "gif", "png");
		$exp = explode(".", $file_name);
		$ext = end($exp);
		$path = "../pict/".$file_name;
		if(in_array($ext, $allowed_ext)){
			if(move_uploaded_file($file_temp, $path)){
				try{
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$insert_query = $db->prepare("INSERT INTO `motor`(`nama_motor`, `plat`, `price`, `img_name`, `loc`) VALUES (?,?,?,?,?)");
					$insert_query->execute([
						$nama, $plat, $price, $file_name, $path
					]);
					}catch(PDOException $e){
						echo $e->getMessage();
					}
					
					$conn = null;
					header('location: ../index.php');
				header('location: index.php');
			
		}
	}
	}


?>