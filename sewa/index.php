<?php require '../connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RC | Shop</title>
    <link rel="icon" href="../pict/icon.png">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
</head>
<body>

    <div class="card">
    <div class="card-header">
        Header
    </div>

    
    <table class="table table-hover">
    <tbody>
        <tr>
            <td>Jenis Kendaraan </td>
            <td><form method="post" action="">
                <select class="form-control" name="kendaraanValue" onchange="if(this.value != 0) { this.form.submit();}">
                    <option value="0">---- PILIH JENIS KENDARAAN ----</option>
                    <option value="1">Motor</option>
                    <option value="2">Mobil</option>
                </select>
                </form>
            </td>
        </tr>
        <form method="post" action="sewa_proses.php">
        <tr>
            <td>Nama Kendaraan </td>
                <?php require_once '../connect.php';
                error_reporting(0);
                    if ($_POST['kendaraanValue'] == '1') {
                ?>
                        <td>
                            <select class="form-control" name="kendaraan" id="kendaraan" aria-placeholder="Kendaraan">
                            <option value="Mio">Mio</option>
                            <option value="Vario">Vario</option>
                            <option value="Beat">Beat</option>
                            </select>
                        </td>
                
                <?php
                    }elseif ($_POST['kendaraanValue'] == '2') {
                ?>
                        <td>
                            <select class="form-control" name="kendaraan" id="kendaraan" aria-placeholder="Kendaraan">
                            <option value="Avanza">Avanza</option>
                            <option value="Ertiga">Ertiga</option>
                            <option value="Rush">Rush</option>
                            </select>
                        </td>

                <?php
                    }else{
                ?>
                        <td>
                            <select class="form-control" name="kendaraan" id="kendaraan" aria-placeholder="Kendaraan">
                            <option value="Avanza">---- Pilih Jenis Kendaraan Terlebih Dahulu -----</option>
                            </select>
                        </td>
                <?php
                }
                ?>
          
        </tr>
        <tr>
            <?php
            $id = $_GET['id'];
            if (!empty($id)) {
            
                $selectQuery = $db->prepare("SELECT * FROM mobil where id = ? ");
                $selectQuery->execute([$id]);

                $ui = $selectQuery->fetch();

               
                    // $name = $ui['nama_kendaraan'];
                
            ?>
            <td>Nama Kendaraan </td>
            <td><input class="form-control" type="text" name="tenggat" value="<?php echo $ui['nama_mobil'] ; ?>"></td>

            <?php
                }
            ?>

        </tr>
        <tr>
            <td>Waktu Sewa </td>
            <td><input class="form-control" type="date" name="tenggat"></td>
        </tr>
        <tr>
            <td>No. Identitas</td>
            <td><input class="form-control" type="text" name="identitas"></td>
        </tr>
        <tr>
            <td>Nomor Telepon</td>
            <td><input class="form-control" type="number" name="nomor"></td>
        </tr>
        <tr>
            <td>Supir</td>
            <td>
                    <input class="checkbox" type="checkbox" value="Pakai" name="supir">Pakai        
            </td>
        </tr>
    </tbody>
    <tfoot>
            <tr>
                <td colspan="3">
                <button type="submit" name="add" class="btn btn-primary" id="formButton">Submit</button>
                </td>
            </tr>
        </tfoot>
    </table>
    </form>
</body>
</html>