<?php require '../connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rumah Cokor | Login</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
    <!-- ------LOGIN-------- -->
<div class="card text-center" id="login">
  <div class="card-header">
    <ul class="nav nav-pills card-header-pills">
      <li class="nav-item">
        <a class="nav-link active">Sign In</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../daftar/index.php">Sign Up</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h5 class="card-title">Silahkan Login terlebih dahulu !!</h5>
    <center>
    <form class="form-group col-md-5" method="post" action="login_proses.php">
        <label for="">Username</label></br>
        <input class="form-control" name="username" type="text">
        <label for="">Password</label></br>
        <input class="form-control" name="password" type="password">
        <button type="sumbit" class="btn btn-primary">Sign In</button>
    </form>
    </center>
  </div>
</div>
</body>
</html>