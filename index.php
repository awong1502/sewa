<?php require 'connect.php';
session_start();
$select_query = $db->query('SELECT * FROM `user`')

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sewa Lonthe</title>
    <link rel="icon" href="pict/icon.png">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
</head>
<body>



<!-- -----Navbar------ -->
<nav class="navbar navbar-expand-lg" id="navbarUtama">
<a class="navbar-brand text-light" style="padding-left: 2rem; padding-top:0.5rem;" href="#">
    <img src="pict/Logo.png" width="50" height="50" class="d-inline-block align-top">
  </a>
  <a class="navbar-brand text-dark font-weight-bold" style="font-size:x-large; padding-left:1rem; padding-top:0.5rem;" href="">RC</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link border border-light rounded-pill text-light" style="padding-left: 3rem; padding-right:3rem;" href="list/index.php">List</a>
      </li>
      <li class="nav-item">
        <a class="nav-link border border-light rounded-pill text-light" style="padding-left: 2rem; padding-right: 2rem;" href="sewa/index.php">Sewa Disini <span class="sr-only">(current)</span></a>
      </li>
      <?php require_once 'connect.php';
      if (!empty($_SESSION['username'])) {
      ?>

      <li class="nav-item ">
        <a class="nav-link border border-light rounded-pill text-light" style="padding-left: 2rem; padding-right:2rem;" id="navLink" href="user/index.php"><?= $_SESSION['username'] ?></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link border border-light rounded-pill text-light" style="padding-left: 2rem; padding-right: 2rem;" id="navLink" href="other/logout.php">logout</a>
      </li>
      <?php
        }else{
      ?>
      <li class="nav-item ">
        <a class="nav-link border border-light rounded-pill text-light" style="padding-left: 3.5rem; padding-right:3.5rem;" id="navLink" href="other/logout.php">Harga</a>
      </li>
      <?php
        }
      ?>
    </ul>
  </div>
</nav>
<!-- ------Navbar(closed)------ -->
<hr>

<div class="cont">
  <div class="card-rounded" id="cardMain" style="width: 50rem; height:32rem; background:transparent;">
    <div class="card-body" id="cardLp">
      <h1 class="card-title text-dark">Rumah Cokor Leasing</h1>
      <h3 class="card-subtitle mb-2 text-light">Penyewaan Motor dan Mobil</h3>
      <p class="card-text text-dark" style="padding-top: 2rem;">Penyewaaan Mobil dan Motor untuk anda yang membutuhkan, kami sangat senang bisa membantu anda</p>
      <a href="daftar/index.php"   class="btn btn-light" style="width: 6rem;">Daftar</a>
      <a href="#" class="btn btn-secondary" style="width: 9rem; margin-left: 2.5rem;">Hubungi Kami</a>
    </div>
  </div>

  <div class="card-rounded" id="cardSecondary" style="width: 44rem; height:35rem; background:transparent;">
    <div class="card-body" id="cardLp">
      <h3 class="card-subtitle mb-2 text-dark" style="padding-bottom: 3rem;">Sudah Punya akun? Yaudah Login sini</h3>
      <form method="post" action="login/login_proses.php" class="formMain">
        <div class="form-group">
          <label for="exampleInputEmail1" class="text-light">Username</label>
          <input type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp">
          <small id="emailHelp" class="form-text text-muted">Santuy email anda aman</small>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1" class="text-light">Password</label>
          <input type="password" name="password" class="form-control" id="exampleInputPassword1">
        </div>
        <button type="submit" class="btn btn-secondary">Submit</button>
      </form>
    </div>
  </div>
</div>
</body>
</html>