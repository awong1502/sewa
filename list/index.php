<?php require '../connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RC | List</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>




    <!-- -------ITEM CONTENT--------- -->
    <section class="container" id="content-1">
    <?php
    require_once '../connect.php';
                    
    $sql = $db->prepare("SELECT * FROM `mobil`");
    $sql->execute();
                                    
    while ($fetch = $sql->fetch()) {
    ?>
    
    <div class="card border-success" id="cardDua" style="width: 18rem;">
        
        <div class="card-header bg-transparent border-success text-success" id="cardTitle">
            <h5 class="card-title" style="padding-bottom: 1rem;"><?= $fetch['nama_mobil'] ?></h5>
        </div>
        <img src="../pict/<?php echo $fetch['img_name'] ?>" class="card-img-top" alt="">
            <div class="border rounded-sm border-success" id="descImg">
                <p class="card-text"><?= $fetch['plat'] ?></p> 
            </div>
        <div class="card-footer bg-transparent border-success" id="footerImg">
            <p class="card-text font-weight-bold" id="harga">Harga : <?= $fetch['price'] ?></p>

            <form class="form" method="post" id="formId" action="update_user.php?id=<?= $fetch['id'] ?>">
            <button class="btn btn-success" id="buttonForm">Masukkan ke keranjang</button>
            </form>
        </div>
    </div>
    
    <?php
        }
    ?>
    
    <?php
    require_once '../connect.php';
                    
    $sql = $db->prepare("SELECT * FROM `motor`");
    $sql->execute();
                                    
    while ($fetch = $sql->fetch()) {
    ?>
    
    <div class="card border-success" id="cardDua" style="width: 18rem;">
        <div class="card-header bg-transparent border-success text-success" id="cardTitle">
            <h5 class="card-title" style="padding-bottom: 1rem;"><?= $fetch['nama_motor'] ?></h5>
        </div>
        <img src="../pict/<?php echo $fetch['img_name'] ?>" class="card-img-top" alt="">
            <div class="border rounded-sm border-success" id="descImg">
                <p class="card-text"><?= $fetch['plat'] ?></p> 
            </div>
        <div class="card-footer bg-transparent border-success" id="footerImg">
            <p class="card-text font-weight-bold" id="harga">Harga : <?= $fetch['price'] ?></p>

            <form class="form" method="get" id="formId" action="update_user.php?id=<?= $fetch['id'] ?>">
                <button class="btn btn-success" id="buttonForm">Masukkan ke keranjang</button>
            </form>

        </div>
    </div>
    
    <?php
        }
    ?>
</body>
</html>